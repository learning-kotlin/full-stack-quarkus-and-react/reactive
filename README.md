# Full Stack Quarkus and React: reactive (backend)

This is a backend sample code for the book _"Full Stack Quarkus and React"_ from Packt Publishing.

## Technologies used

This project uses Quarkus, the Supersonic Subatomic Java Framework. 
Project is build with Gradle and code is written with Kotlin.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Generating security key files

The application is using JWT for authorisation and authentication and it is configured to use RSA security keys 
to encrypt and sign the token. For this need to execute following command in `src/main/resources/jwt` directory:

```shell
openssl genrsa -out rsa-private-key.pem 2048
```

This will generate master private key. This key should be kept secret and should never be shared.

```shell
openssl pkcs8 -topk8 -nocrypt -inform pem -in rsa-private-key.pem -outform pem -out private-key.pem
```

This command will convert RSA private key into **PKCS#8** format to make it compatible with the SmallRye JWT build.

```shell
openssl rsa -pubout -in rsa-private-key.pem -out public-key.pem
```

This will generate the public key.

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./gradlew quarkusDev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:

```shell script
./gradlew build
```

It produces the `quarkus-run.jar` file in the `build/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/quarkus-app/lib/` directory.

The application is now runnable using `java -jar build/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./gradlew build -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar build/*-runner.jar`.

## Creating a native executable

You can create a native executable using:

```shell script
./gradlew build -Dquarkus.package.type=native
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./gradlew build -Dquarkus.package.type=native -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./build/reactive-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/gradle-tooling.

## Related Guides

- RESTEasy Reactive ([guide](https://quarkus.io/guides/resteasy-reactive)): A Jakarta REST implementation utilizing
  build time processing and Vert.x. This extension is not compatible with the quarkus-resteasy extension, or any of the
  extensions that depend on it.
- Kotlin ([guide](https://quarkus.io/guides/kotlin)): Write your services in Kotlin

## License

[License](./LICENSE)
