package org.elu.learn.quarkus.fullstack

import io.vertx.pgclient.PgException
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.ext.ExceptionMapper
import jakarta.ws.rs.ext.Provider
import org.hibernate.HibernateException
import org.hibernate.ObjectNotFoundException
import org.hibernate.StaleObjectStateException

@Provider
class RestExceptionHandler : ExceptionMapper<HibernateException> {
    override fun toResponse(exception: HibernateException?): Response =
        if (hasExceptionInChain(exception, ObjectNotFoundException::class.java)) {
            Response
                .status(Response.Status.NOT_FOUND)
                .entity(exception?.message)
                .build()
        } else if (hasExceptionInChain(exception, StaleObjectStateException::class.java)
            || hasPostgresErrorCode(exception, PG_UNIQUE_VIOLATION_ERROR)) {
            Response
                .status(Response.Status.CONFLICT)
                .build()
        } else {
            Response
                .status(Response.Status.BAD_REQUEST)
                .entity("\"${exception?.message}\"")
                .build()
        }

    companion object {
        private const val PG_UNIQUE_VIOLATION_ERROR = "23505"

        private fun hasExceptionInChain(throwable: Throwable?, exceptionClass: Class<out Throwable>) =
            getExceptionInChain(throwable, exceptionClass)?.let { true } ?: false

        private fun hasPostgresErrorCode(throwable: Throwable?, code: String) =
            getExceptionInChain(throwable, PgException::class.java)
                ?.let { ex -> ex is PgException && ex.sqlState == code }
                ?: false

        private fun getExceptionInChain(
            throwable: Throwable?,
            exceptionClass: Class<out Throwable>
        ): Throwable? {
            var ex = throwable
            while (ex != null) {
                if (exceptionClass.isInstance(ex)) {
                    return ex
                }
                ex = ex.cause
            }
            return null
        }
    }
}
