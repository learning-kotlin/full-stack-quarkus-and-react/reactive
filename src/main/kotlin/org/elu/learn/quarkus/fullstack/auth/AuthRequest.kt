package org.elu.learn.quarkus.fullstack.auth

data class AuthRequest(val name: String, val password: String)
