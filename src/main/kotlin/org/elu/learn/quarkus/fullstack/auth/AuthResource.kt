package org.elu.learn.quarkus.fullstack.auth

import io.smallrye.mutiny.Uni
import jakarta.annotation.security.PermitAll
import jakarta.inject.Inject
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path

@Path("/api/v1/auth")
class AuthResource {
    @Inject
    private lateinit var authService: AuthService

    @PermitAll
    @POST
    @Path("/login")
    fun login(request: AuthRequest): Uni<String>? = authService.authenticate(request)
}
