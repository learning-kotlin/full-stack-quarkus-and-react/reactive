package org.elu.learn.quarkus.fullstack.auth

import io.quarkus.security.AuthenticationFailedException
import io.smallrye.jwt.build.Jwt
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.unchecked.Unchecked
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.elu.learn.quarkus.fullstack.user.UserService
import java.time.Duration

@ApplicationScoped
class AuthService {
    @ConfigProperty(name = "mp.jwt.verify.issuer")
    private lateinit var issuer: String

    @Inject
    private lateinit var userService: UserService

    fun authenticate(authRequest: AuthRequest): Uni<String>? =
        userService.findByName(authRequest.name)
            ?.onItem()
            ?.transform(Unchecked.function { user ->
                if (user == null || !UserService.matches(user, authRequest.password)) {
                    throw AuthenticationFailedException("invalid credentials")
                } else {
                    Jwt.issuer(issuer)
                        .upn(user.name)
                        .groups(HashSet(user.roles))
                        .expiresIn(Duration.ofHours(1L))
                        .sign()
                }
            })
}
