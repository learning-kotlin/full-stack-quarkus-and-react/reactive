package org.elu.learn.quarkus.fullstack.project

import jakarta.persistence.*
import org.elu.learn.quarkus.fullstack.user.User
import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime

@Entity
@Table(
    name = "projects",
    uniqueConstraints = [
        UniqueConstraint(columnNames = [ "name", "user_id" ])
    ]
)
class Project {
    @Id
    @GeneratedValue
    var id: Long? = null

    @Column(nullable = false)
    lateinit var name: String

    @ManyToOne(optional = false)
    lateinit var user: User

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    lateinit var created: ZonedDateTime

    @Version
    var version: Int = 0
}
