package org.elu.learn.quarkus.fullstack.project

import io.quarkus.hibernate.reactive.panache.PanacheRepository
import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class ProjectRepository: PanacheRepository<Project> {
}
