package org.elu.learn.quarkus.fullstack.project

import jakarta.annotation.security.RolesAllowed
import jakarta.inject.Inject
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.core.MediaType
import org.jboss.resteasy.reactive.ResponseStatus

@Path("/api/v1/projects")
@RolesAllowed("user")
class ProjectResource {
    @Inject
    private lateinit var projectService: ProjectService

    @GET
    fun get() = projectService.listForUser()

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseStatus(201)
    fun create(project: Project) = projectService.create(project)

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    fun update(@PathParam("id") id: Long, project: Project) = if (project.id == id) {
        projectService.update(project)
    } else {
        throw IllegalArgumentException("Try to update wrong entity")
    }

    @DELETE
    @Path("{id}")
    fun delete(@PathParam("id") id: Long) = projectService.delete(id)
}
