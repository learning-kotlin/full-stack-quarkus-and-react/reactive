package org.elu.learn.quarkus.fullstack.project

import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.security.UnauthorizedException
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.elu.learn.quarkus.fullstack.task.TaskRepository
import org.elu.learn.quarkus.fullstack.user.UserService
import org.hibernate.ObjectNotFoundException

@ApplicationScoped
class ProjectService {
    @Inject
    private lateinit var userService: UserService
    @Inject
    private lateinit var projectRepository: ProjectRepository
    @Inject
    private lateinit var taskRepository: TaskRepository

    fun findById(id: Long): Uni<Project> = userService.getCurrentUser()
        .chain { user ->  projectRepository.findById(id)
            .onItem().ifNull().failWith { ObjectNotFoundException(id, "Project") }
            .onItem().invoke { project ->
                run {
                    if (project.user.id != user.id) {
                        throw UnauthorizedException("You are not allowed to update this project")
                    }
                }
            }
        }

    fun listForUser(): Uni<List<Project>> = userService.getCurrentUser()
        .chain { user -> projectRepository.find("user", user).list() }

    @WithTransaction
    fun create(project: Project): Uni<Project> = userService.getCurrentUser()
        .chain { user ->
            project.user = user
            projectRepository.persistAndFlush(project)
        }

    @WithTransaction
    fun update(project: Project): Uni<Project> = project.id?.let {
        findById(it)
            .chain { _ -> projectRepository.session }
            .chain { s -> s.merge(project) }
    } ?: throw IllegalArgumentException("Missing ID")

    @WithTransaction
    fun delete(id: Long): Uni<Boolean> = findById(id)
        .chain { p -> taskRepository.update("project = null where project = ?1", p) }
        .chain { _ -> projectRepository.deleteById(id) }
}
