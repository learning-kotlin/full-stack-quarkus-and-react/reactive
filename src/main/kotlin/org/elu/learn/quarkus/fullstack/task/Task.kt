package org.elu.learn.quarkus.fullstack.task

import jakarta.persistence.*
import org.elu.learn.quarkus.fullstack.project.Project
import org.elu.learn.quarkus.fullstack.user.User
import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime

@Entity
@Table(name = "tasks")
class Task {
    @Id
    @GeneratedValue
    var id: Long? = null

    @Column(nullable = false)
    lateinit var title: String

    @Column(length = 1000)
    lateinit var description: String

    var priority: Int = 0

    @ManyToOne(optional = false)
    lateinit var user: User

    var complete: ZonedDateTime? = null

    @ManyToOne
    lateinit var project: Project

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    lateinit var created: ZonedDateTime

    @Version
    var version: Int = 0
}
