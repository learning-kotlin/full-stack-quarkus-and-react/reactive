package org.elu.learn.quarkus.fullstack.task

import io.quarkus.hibernate.reactive.panache.PanacheRepository
import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class TaskRepository: PanacheRepository<Task> {
}
