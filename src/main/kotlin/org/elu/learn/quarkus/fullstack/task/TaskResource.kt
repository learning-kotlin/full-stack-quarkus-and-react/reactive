package org.elu.learn.quarkus.fullstack.task

import jakarta.annotation.security.RolesAllowed
import jakarta.inject.Inject
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.core.MediaType
import org.jboss.resteasy.reactive.ResponseStatus

@Path("/api/v1/tasks")
@RolesAllowed("user")
class TaskResource {
    @Inject
    private lateinit var taskService: TaskService

    @GET
    fun get() = taskService.listForUser()

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseStatus(201)
    fun create(task: Task) = taskService.create(task)

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    fun update(@PathParam("id") id: Long, task: Task) = if (task.id == id) {
        taskService.update(task)
    } else {
        throw IllegalArgumentException("Try to update wrong entity")
    }

    @DELETE
    @Path("{id}")
    fun delete(@PathParam("id") id: Long) = taskService.delete(id)
}
