package org.elu.learn.quarkus.fullstack.task

import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.quarkus.security.UnauthorizedException
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import org.elu.learn.quarkus.fullstack.user.UserService
import org.hibernate.ObjectNotFoundException
import java.time.ZonedDateTime

@ApplicationScoped
class TaskService {
    @Inject
    private lateinit var userService: UserService

    @Inject
    private lateinit var taskRepository: TaskRepository

    fun findById(id: Long): Uni<Task> = userService.getCurrentUser()
        .chain { user ->
            taskRepository.findById(id)
                .onItem().ifNull().failWith { ObjectNotFoundException(id, "Task") }
                .onItem().invoke { task ->
                    run {
                        if (user.id != task.user.id) {
                            throw UnauthorizedException("You are not allowed to update this task")
                        }
                    }
                }
        }

    fun listForUser(): Uni<List<Task>> = userService.getCurrentUser()
        .chain { user -> taskRepository.find("user", user).list() }

    @WithTransaction
    fun create(task: Task): Uni<Task> = userService.getCurrentUser()
        .chain { user ->
            task.user = user
            taskRepository.persistAndFlush(task)
        }

    @WithTransaction
    fun update(task: Task): Uni<Task> = task.id?.let {
        findById(it)
            .chain { _ -> taskRepository.session }
            .chain { s -> s.merge(task) }
    } ?: throw IllegalArgumentException("Missing ID")

    @WithTransaction
    fun delete(id: Long): Uni<Boolean> = findById(id)
        .chain {t -> taskRepository.deleteById(t.id) }

    @WithTransaction
    fun setComplete(id: Long, complete: Boolean): Uni<Boolean> = findById(id)
        .chain { task ->
            task.complete = if (complete) ZonedDateTime.now() else null
            taskRepository.persistAndFlush(task)
        }
        .chain { _ -> Uni.createFrom().item(complete) }
}
