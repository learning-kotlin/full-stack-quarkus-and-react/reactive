package org.elu.learn.quarkus.fullstack.user

data class PasswordChange(val currentPassword: String, val newPassword: String)
