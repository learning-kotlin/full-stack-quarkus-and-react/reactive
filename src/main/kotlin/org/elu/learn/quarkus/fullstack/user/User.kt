package org.elu.learn.quarkus.fullstack.user

import jakarta.persistence.*
import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime

@Entity
@Table(name = "users")
class User {
    @Id
    @GeneratedValue
    var id: Long? = null

    @Column(unique = true, nullable = false)
    lateinit var name: String

    @Column(nullable = false)
    lateinit var password: String

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    lateinit var created: ZonedDateTime

    @Version
    var version: Int = 0

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "user_roles", joinColumns = [JoinColumn(name = "id")])
    @Column(name = "role")
    lateinit var roles: List<String>
}
