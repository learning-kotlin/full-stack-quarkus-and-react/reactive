package org.elu.learn.quarkus.fullstack.user

import java.time.ZonedDateTime

data class UserDTO(
    val id: Long?,
    val name: String,
    val password: String? = null,
    val created: ZonedDateTime? = null,
    val version: Int? = 0,
    val roles: List<String>? = listOf(),
)
