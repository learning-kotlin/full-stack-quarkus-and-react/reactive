package org.elu.learn.quarkus.fullstack.user

import io.quarkus.hibernate.reactive.panache.PanacheRepository
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class UserRepository: PanacheRepository<User> {
    fun findByName(name: String): Uni<User> = find("name", name).firstResult()
}
