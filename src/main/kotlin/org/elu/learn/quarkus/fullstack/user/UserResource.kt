package org.elu.learn.quarkus.fullstack.user

import io.smallrye.mutiny.Uni
import jakarta.annotation.security.RolesAllowed
import jakarta.inject.Inject
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.POST
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.core.MediaType
import org.jboss.resteasy.reactive.ResponseStatus

@Path("/api/v1/users")
class UserResource {
    @Inject
    private lateinit var userService: UserService

    @GET
    fun get(): Uni<List<UserDTO>> = userService.list()

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ResponseStatus(201)
    fun create(user: UserDTO): Uni<UserDTO> = userService.create(user)

    @GET
    @Path("{id}")
    fun get(@PathParam("id") id: Long) = userService.findById(id)

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    fun update(@PathParam("id") id: Long, user: UserDTO) = if (user.id == id) {
        userService.update(user)
    } else {
        throw IllegalArgumentException("Try to update wrong entity")
    }

    @DELETE
    @Path("{id}")
    fun delete(@PathParam("id") id: Long) = userService.delete(id)

    @GET
    @Path("{self}")
    fun getCurrentUser() = userService.currentUser()

    @PUT
    @Path("self/password")
    @RolesAllowed("user")
    fun changePassword(passwordChange: PasswordChange) =
        userService.changePassword(passwordChange.currentPassword, passwordChange.newPassword)
}
