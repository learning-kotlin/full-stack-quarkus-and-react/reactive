package org.elu.learn.quarkus.fullstack.user

import io.quarkus.elytron.security.common.BcryptUtil
import io.quarkus.hibernate.reactive.panache.common.WithSession
import io.quarkus.hibernate.reactive.panache.common.WithTransaction
import io.smallrye.mutiny.Uni
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import jakarta.ws.rs.ClientErrorException
import jakarta.ws.rs.core.Response
import org.eclipse.microprofile.jwt.JsonWebToken
import org.elu.learn.quarkus.fullstack.project.ProjectRepository
import org.elu.learn.quarkus.fullstack.task.TaskRepository
import org.hibernate.ObjectNotFoundException

@ApplicationScoped
class UserService {
    @Inject
    private lateinit var userRepository: UserRepository

    @Inject
    private lateinit var projectRepository: ProjectRepository

    @Inject
    private lateinit var taskRepository: TaskRepository

    @Inject
    private lateinit var jwt: JsonWebToken

    @WithSession
    fun findById(id: Long): Uni<UserDTO> = userRepository.findById(id)
        .onItem()
        .ifNull().failWith { ObjectNotFoundException(id, "User") }
        .map { mapToDto(it) }

    @WithSession
    fun findByName(name: String): Uni<User> = userRepository.findByName(name)

    @WithSession
    fun list(): Uni<List<UserDTO>> = userRepository.listAll()
        .map {
            it.map { entity -> mapToDto(entity) }
        }

    @WithTransaction
    fun create(user: UserDTO): Uni<UserDTO> {
        val userEntity = User()
        userEntity.name = user.name
        userEntity.password = BcryptUtil.bcryptHash(user.password)
        userEntity.roles = user.roles ?: listOf()
        return userRepository.persistAndFlush(userEntity)
            .map { mapToDto(it) }
    }

    @WithTransaction
    fun update(user: UserDTO): Uni<UserDTO> = userRepository.findById(user.id)
        .chain { u ->
            u.name = user.name
            u.roles = user.roles ?: listOf()
            userRepository.persistAndFlush(u)
        }.map { mapToDto(it) }

    @WithTransaction
    fun delete(id: Long): Uni<Boolean> = userRepository.findById(id)
        .chain { u ->
            Uni.combine().all().unis(
                taskRepository.delete("user.id", u.id),
                projectRepository.delete("user.id", u.id),
            ).asTuple()
                .chain { _ -> userRepository.deleteById(u.id) }
        }

    @WithSession
    fun getCurrentUser(): Uni<User> = findByName(jwt.name)

    fun currentUser(): Uni<UserDTO> = getCurrentUser().map { mapToDto(it) }

    @WithTransaction
    fun changePassword(currentPassword: String, newPassword: String): Uni<UserDTO> =
        getCurrentUser().chain { u ->
            if (matches(u, currentPassword)) {
                u.password = BcryptUtil.bcryptHash(newPassword)
                userRepository.persistAndFlush(u)
            } else {
                throw ClientErrorException("Current password does not match", Response.Status.CONFLICT)
            }
        }.map { mapToDto(it) }

    private fun mapToDto(user: User): UserDTO = UserDTO(
        id = user.id,
        name = user.name,
        created = user.created,
        version = user.version,
        roles = user.roles
    )

    companion object {
        fun matches(user: User, password: String) = BcryptUtil.matches(password, user.password)
    }
}
